//
//  ChallengeDetailViewController.swift
//  Apple Challnge 2
//
//  Created by Andre Marines ado Tena Uak on 15/04/20.
//  Copyright © 2020 Andre Marines ado Tena Uak. All rights reserved.
//

import UIKit

class ChallengeDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    

    var detail : [Detail] = []
    var datachallenge = [String]()
    
    @IBOutlet weak var tabledetail: UITableView!
    
    var data = [String]()
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        detail = createArray()
        
        tabledetail.delegate = self
        tabledetail.dataSource = self

    }
    
    func createArray () -> [Detail]{

        var tempData: [Detail] = []

        let detail1 = Detail(image: UIImage(named: "challenge1")!, name_Challenge: "Challenge 1", type_Challenge: "Team Challenge", lama_Challenge: "5 Week", penjelasan: "Challenge perkenalan awal", tujuan: "Perkenalan", constrains: "NO Game")
//
//        let detail2 = Detail(image: UIImage(named: "challenge2")!, name_Challenge: "Challenge 2", type_Challenge: "Individual Challenge", lama_Challenge: "2 Week", penjelasan: "Challenge perkenalan awal", tujuan: "Perkenalan", constrains: "NO Game")
//
//        let detail3 = Detail(image: UIImage(named: "challenge3")!, name_Challenge: "Challenge 3", type_Challenge: "Team Challenge", lama_Challenge: "4 Week", penjelasan: "Challenge perkenalan awal", tujuan: "Perkenalan", constrains: "NO Game")
//
//        let detail4 = Detail(image: UIImage(named: "challenge4")!, name_Challenge: "Challenge 4", type_Challenge: "Team Challenge", lama_Challenge: "4 Week", penjelasan: "TBA", tujuan: "Perkenalan", constrains: "NO Game")
//
//        let detail5 = Detail(image: UIImage(named: "challenge5")!, name_Challenge: "Challenge 5", type_Challenge: "Individual Challenge", lama_Challenge: "2 Week", penjelasan: "TBA", tujuan: "Perkenalan", constrains: "NO Game")
//
//        let detail6 = Detail(image: UIImage(named: "challenge6")!, name_Challenge: "Challenge 6", type_Challenge: "Team Challenge", lama_Challenge: "12 Week", penjelasan: "TBA", tujuan: "Perkenalan", constrains: "NO Game")

        tempData.append(detail1)
//        tempData.append(detail2)
//        tempData.append(detail3)
//        tempData.append(detail4)
//        tempData.append(detail5)
//        tempData.append(detail6)

        return tempData
        
    }
        
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return detail.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data2 = detail[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCell") as! DatailChallengeTableViewCell
        
        cell.setDetailData(data: data2)
        
                return cell
        
    }
    
}
    

    
    



