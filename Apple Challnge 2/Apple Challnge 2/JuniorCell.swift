//
//  JuniorCell.swift
//  Apple Challnge 2
//
//  Created by Andre Marines ado Tena Uak on 22/04/20.
//  Copyright © 2020 Andre Marines ado Tena Uak. All rights reserved.
//

import UIKit

class JuniorCell: UITableViewCell {

    @IBOutlet weak var GambarRoket: UIImageView!
    @IBOutlet weak var Junior: UILabel!

    func setdata(data: Explorers){
        GambarRoket.image = data.gambar
        Junior.text = data.kategori
    }

}
