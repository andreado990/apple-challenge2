//
//  ListExplorersCell.swift
//  Apple Challnge 2
//
//  Created by Andre Marines ado Tena Uak on 22/04/20.
//  Copyright © 2020 Andre Marines ado Tena Uak. All rights reserved.
//

import UIKit

class ListExplorersCell: UITableViewCell {

    @IBOutlet weak var GambarRoket: UIImageView!
    @IBOutlet weak var Explorers: UILabel!
    
    func setDetail(data : Explorers){
        GambarRoket.image = data.gambar
        Explorers.text = data.kategori
    }

}
