//
//  SeniorJourney.swift
//  Apple Challnge 2
//
//  Created by Andre Marines ado Tena Uak on 21/04/20.
//  Copyright © 2020 Andre Marines ado Tena Uak. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class SeniorJourney: UICollectionViewController {
    
    var dataSenior = [[String:String]]()
    
    func loadPlist () -> [[String:String]]{
        let path = Bundle.main.path(forResource: "SeniorList", ofType: "plist")
        return NSArray.init(contentsOf: URL.init(fileURLWithPath: path!)) as! [[String:String]]
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.dataSenior = loadPlist()
    }

//    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
//        if segue.identifier == "ShowData"{
//            let detail = segue.destination as! DetailSeniorExplorers
//
//            if let indexPath = self.collectionView?.indexPath(for: UICollectionViewCell){
//
//                let data = self.dataSenior[indexPath.row]
//
//            }
//
//
//        }
//    }

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return self.dataSenior.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SeniorCell", for: indexPath) as! SeniorCell
    
        let person = self.dataSenior[indexPath.row]
        
        cell.PhotoSenior.image = UIImage(named: person["New item - 3"]!)
        cell.NameSenior.text = person["New item"]
        cell.JabatanSenior.text = person["new item - 2"]
            

    
        return cell
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
