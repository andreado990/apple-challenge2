// =======================================================
// This file is auto generated by [DataGenerator for Excel Lite] convertor,
// do not edit by youself!
// >>>> by HuMinghua <<<<  15 Apr 2020 16.34.12
// ======================================================


import Foundation


private let AUTO_PLIST_DATAEXPLORERS_PATH = Bundle.main.path(forResource: "dataExplorers", ofType: "plist")

private let AUTO_PLIST_DATAEXPLORERS_INDEX_NAME = 0   // Explorer
private let AUTO_PLIST_DATAEXPLORERS_INDEX_SENIOR_JUNIOR = 1   // 
private let AUTO_PLIST_DATAEXPLORERS_INDEX_SHIFT = 2   // 
private let AUTO_PLIST_DATAEXPLORERS_INDEX_ROLE = 3   // 
private let AUTO_PLIST_DATAEXPLORERS_INDEX_PHOTO = 4   // 
private let AUTO_PLIST_DATAEXPLORERS_INDEX_WANT_TO_LEARN = 5   // 
private let AUTO_PLIST_DATAEXPLORERS_INDEX_CAN_HELP_WITH = 6   // 


class DataExplorersManager {

    public static let sharedManager = DataExplorersManager()
    private init(){}

    public var _dataExplorerss = [DataExplorers]()

    func allDataExplorerss() -> [DataExplorers]{

        if _dataExplorerss.count > 0 {
            return _dataExplorerss
        }

        if let allDatas = NSArray(contentsOfFile: AUTO_PLIST_DATAEXPLORERS_PATH!) {

            for array in allDatas {
                guard let array = array as? [AnyObject] else {continue}

                let dataExplorers = DataExplorers()
                dataExplorers.name = array[AUTO_PLIST_DATAEXPLORERS_INDEX_NAME] as? String // Explorer
                dataExplorers.senior_Junior = array[AUTO_PLIST_DATAEXPLORERS_INDEX_SENIOR_JUNIOR] as? String // 
                dataExplorers.shift = array[AUTO_PLIST_DATAEXPLORERS_INDEX_SHIFT] as? String // 
                dataExplorers.role = array[AUTO_PLIST_DATAEXPLORERS_INDEX_ROLE] as? String // 
                dataExplorers.photo = array[AUTO_PLIST_DATAEXPLORERS_INDEX_PHOTO] as? String // 
                dataExplorers.want_to_learn = array[AUTO_PLIST_DATAEXPLORERS_INDEX_WANT_TO_LEARN] as? String // 
//                dataExplorers.can_help_with = array[AUTO_PLIST_DATAEXPLORERS_INDEX_CAN_HELP_WITH] as? String //
                _dataExplorerss.append(dataExplorers)
            }
        }
        return _dataExplorerss
    }
}
