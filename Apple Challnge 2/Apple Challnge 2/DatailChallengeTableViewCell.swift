//
//  DatailChallengeTableViewCell.swift
//  Apple Challnge 2
//
//  Created by Andre Marines ado Tena Uak on 17/04/20.
//  Copyright © 2020 Andre Marines ado Tena Uak. All rights reserved.
//

import UIKit

class DatailChallengeTableViewCell: UITableViewCell {

    @IBOutlet weak var ImageChallenge: UIImageView!
    @IBOutlet weak var GoalsChallenge: UILabel!
    @IBOutlet weak var TypeChallenge: UILabel!
    @IBOutlet weak var DescriptionChallenge: UILabel!
    @IBOutlet weak var NameChallenge: UILabel!
    @IBOutlet weak var TimeChallenge: UILabel!
    @IBOutlet weak var ConstrainsChallenge: UILabel!
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        // Initialization code
//    }

    func setDetailData (data : Detail ){
        ImageChallenge.image = data.image
        GoalsChallenge.text = data.tujuan
        TypeChallenge.text = data.type_Challenge
        DescriptionChallenge.text = data.penjelasan
        NameChallenge.text = data.name_Challenge
        TimeChallenge.text = data.lama_Challenge
        ConstrainsChallenge.text = data.constrains
    }
}
