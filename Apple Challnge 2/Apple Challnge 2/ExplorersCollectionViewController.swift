//
//  ExplorersCollectionViewController.swift
//  Apple Challnge 2
//
//  Created by Andre Marines ado Tena Uak on 15/04/20.
//  Copyright © 2020 Andre Marines ado Tena Uak. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class ExplorersCollectionViewController: UICollectionViewController {

//    let coba = DataExplorersManager.sharedManager.allDataExplorerss()
    var dataperson = [[String:String]]()
//    var data = [String]()
    
    func loadPist () -> [[String:String]]{
        
//        let path = Bundle.main.path(forResource: "coba-coba", ofType: "plist")
        let path = Bundle.main.path(forResource: "dataExplorers", ofType: "plist")
//        let dict = NSDictionary(contentsOfFile: path!)
        
//        dataperson = dict!.object(forKey: "Root") as! [String]
        
        return NSArray.init(contentsOf: URL.init(fileURLWithPath: path!)) as! [[String:String]]
//        return NSDictionary.init(contentsOf: URL.init(fileURLWithPath: path!)) as! [String]
        
    }
    
//    func loadPlist () -> [String]{
//        let path = Bundle.main.path(forResource: "coba-coba", ofType: "plist")
//        let dict = NSDictionary(contentsOfFile: path!)
//        data = dict!.object(forKey: "Root") as! [String]
//
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        self.dataperson = loadPist()
        
        // Do any additional setup after loading the view.
        
//        let path = Bundle.main.path(forResource: "coba-coba", ofType: "plist")
//        let dict = NSDictionary(contentsOfFile: path!)
//        data = dict!.object(forKey: "New Item") as! [String]

    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        
        if segue.identifier == "ShowData"{
             
            let detail = segue.destination as! DetailDataExplorers
            
            if let indexPath = self.collectionView?.indexPath(for: sender as! UICollectionViewCell) {
            
                let person2 = self.dataperson[indexPath.row]
                
                detail.sendData1 = person2["New item"]     //nama
                detail.sendData2 = person2["New item - 3"] //Shift
                detail.sendData3 = person2["New item - 5"] //foto
                detail.sendData4 = person2["New item - 6"] //kelebihan
                detail.sendData5 = person2["New item - 7"] //kekurangan
                
                
            }
        
        }
    }
        

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return self.dataperson.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExplorerCell", for: indexPath) as! ExplorersCell
    
        // Configure the cell

        let person = self.dataperson[indexPath.row]
        cell.journeyimagecell.image = UIImage(named: person["New item - 5"]!)
        cell.journeynamecell2.text = person["New item"]
        cell.journeyexpretcell.text = person["New item - 3"]
        
        return cell
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
