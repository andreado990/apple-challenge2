//
//  ExplorersController.swift
//  Apple Challnge 2
//
//  Created by Andre Marines ado Tena Uak on 22/04/20.
//  Copyright © 2020 Andre Marines ado Tena Uak. All rights reserved.
//

import UIKit

class ExplorersController: UIViewController {

    
    @IBOutlet weak var tablekategori: UITableView!
    var data : [Explorers] = []
    var data2 : [Explorers] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        data = createArray()
        data2 = createArray()
        
        tablekategori.delegate = self
        tablekategori.dataSource = self
    }
    
    func createArray() -> [Explorers]{
        var tempData : [Explorers] = []
        
        let data1 = Explorers(gambar: UIImage(named: "Roket2")!, kategori: "Senior Explorers")
        let data2 = Explorers(gambar: UIImage(named: "Roket2")!, kategori: "Junior Explorers")

        
        tempData.append(data1)
        tempData.append(data2)

        
        return tempData
    }
    

}

extension ExplorersController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let table = data[indexPath.row]

        let cell = tableView.dequeueReusableCell(withIdentifier: "DataExplorers") as! ListExplorersCell

        cell.setDetail(data: table)

        
        return cell
    }
    
//    private func tableView(_ tableView: UITableView, didSelectRowAt indexPath: NSIndexPath) {
//        let data3 = display[indexPath.row]
//        
//        let show = storyboard?.instantiateViewController(withIdentifier: data3)
//        self.navigationController?.pushViewController(show!, animated: true)
//    }
    
    
}
 
