//
//  DetailDataExplorers.swift
//  Apple Challnge 2
//
//  Created by Andre Marines ado Tena Uak on 21/04/20.
//  Copyright © 2020 Andre Marines ado Tena Uak. All rights reserved.
//

import UIKit

class DetailDataExplorers: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate{

    @IBOutlet weak var PhotoExplorers: UIImageView!
    @IBOutlet weak var NamaExplorers: UILabel!
    @IBOutlet weak var ShiftExplorers: UILabel!
    @IBOutlet weak var KelebihanExplorers: UITextView!
    @IBOutlet weak var KekuranganExplorers: UITextView!
    @IBOutlet weak var SimiliarExplorers: UICollectionView!
    
    
    var sendData1 : String!
    var sendData2 : String!
    var sendData3 : String!
    var sendData4 : String!
    var sendData5 : String!
    var sendData6 : String!
    
    var dataperson = [[String:String]]()
    
    func loadPlist () -> [[String:String]]{
        
        let path = Bundle.main.path(forResource: "dataExplorers", ofType: "plist")
        
        return NSArray.init(contentsOf: URL.init(fileURLWithPath: path!)) as! [[String:String]]
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        PhotoExplorers.image = UIImage(named: sendData3)
        NamaExplorers.text = sendData1
        ShiftExplorers.text = sendData2
        KelebihanExplorers.text = sendData4
        KekuranganExplorers.text = sendData5
        
        self.dataperson = loadPlist()

    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataperson.count
    }
     
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "JuniorCell2", for: indexPath) as! JuniorCell2
        
        let person = self.dataperson[indexPath.row]
        cell.gambar.image = UIImage(named: person["New item - 5"]!)
        cell.Nama.text = person["New item"]
        cell.Shift.text = person["New item - 3"]
        
        return cell
    }
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
//
//        if segue.identifier == "ShowData"{
//
//            let detail = segue.destination as! DetailDataExplorers
//
//
//            if let indexPath = self.view?.indexPath(for: sender as! UICollectionViewCell) {
//
//            let person2 = self.dataperson[indexPath.row]
//
//                detail.sendData1 = person2["New item"]     //nama
//                detail.sendData2 = person2["New item - 3"] //Shift
//                detail.sendData3 = person2["New item - 5"] //foto
//                detail.sendData4 = person2["New item - 6"] //kelebihan
//                detail.sendData5 = person2["New item - 7"] //kekurangan
//
//
//            }
//
//        }
//    }

}
