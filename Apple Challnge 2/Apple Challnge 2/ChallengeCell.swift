//
//  ChallengeCell.swift
//  Apple Challnge 2
//
//  Created by Andre Marines ado Tena Uak on 14/04/20.
//  Copyright © 2020 Andre Marines ado Tena Uak. All rights reserved.
//

import UIKit

class ChallengeCell: UITableViewCell {

    @IBOutlet weak var ChallengeImage: UIImageView!
    @IBOutlet weak var ChallengeTitle: UILabel!
    
    func setDetail(data : Data){
        ChallengeImage.image = data.image
        ChallengeTitle.text = data.title

    }
    
    

}
