//
//  ExplorersData.swift
//  Apple Challnge 2
//
//  Created by Andre Marines ado Tena Uak on 22/04/20.
//  Copyright © 2020 Andre Marines ado Tena Uak. All rights reserved.
//

import Foundation
import UIKit

class Explorers{
    
    var gambar : UIImage?
    var kategori : String!
    
    init(gambar : UIImage, kategori : String ) {
        self.gambar = gambar
        self.kategori = kategori
    }
}
