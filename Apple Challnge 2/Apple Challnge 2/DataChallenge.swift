//
//  DataChallenge.swift
//  Apple Challnge 2
//
//  Created by Andre Marines ado Tena Uak on 14/04/20.
//  Copyright © 2020 Andre Marines ado Tena Uak. All rights reserved.
//

import Foundation
import UIKit

struct Data{
    
    var image: UIImage?
    var title: String   //nama
    var waktu : String
    var penjelasan : String
    var tipe : String
    var goals : String
    var constrains : String
    
    init(image: UIImage, title: String, waktu: String, penjelasan: String, tipe: String, goals: String, constrains: String){
        self.image = image
        self.title = title
        self.waktu = waktu
        self.penjelasan = penjelasan
        self.tipe = tipe
        self.goals = goals
        self.constrains = constrains
    }
}


