//
//  testViewController.swift
//  Apple Challnge 2
//
//  Created by Andre Marines ado Tena Uak on 17/04/20.
//  Copyright © 2020 Andre Marines ado Tena Uak. All rights reserved.
//

import UIKit

class testViewController: UIViewController {

    @IBOutlet weak var NamaChallenge: UILabel!
    @IBOutlet weak var GambarChallenge: UIImageView!
    @IBOutlet weak var TypeChallenge: UILabel!
    @IBOutlet weak var TimeChallenge: UILabel!
    @IBOutlet weak var PenjelasanChallenge: UITextView!
    @IBOutlet weak var GoalsChallenge: UITextView!
    @IBOutlet weak var ConstrainsChallenge: UITextView!
    
    var image = UIImage()
    var nama = ""
    var waktu = ""
    var penjelasan = ""
    var tipe = ""
    var goals = ""
    var constrains = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        GambarChallenge.image = image
        NamaChallenge.text = nama
        TypeChallenge.text = tipe
        TimeChallenge.text = waktu
        PenjelasanChallenge.text = penjelasan
        GoalsChallenge.text = goals
        ConstrainsChallenge.text = constrains
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
