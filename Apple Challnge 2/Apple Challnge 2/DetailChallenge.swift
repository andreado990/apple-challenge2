//
//  DetailChallenge.swift
//  Apple Challnge 2
//
//  Created by Andre Marines ado Tena Uak on 15/04/20.
//  Copyright © 2020 Andre Marines ado Tena Uak. All rights reserved.
//

import Foundation
import UIKit


class Detail {
    
    var image : UIImage?
    var name_Challenge : String
    var type_Challenge : String
    var lama_Challenge : String
    var penjelasan : String
    var tujuan : String
    var constrains : String
    
    
    init(image: UIImage, name_Challenge: String, type_Challenge: String, lama_Challenge: String, penjelasan: String, tujuan: String, constrains: String){
        self.image = image
        self.name_Challenge = name_Challenge
        self.type_Challenge = type_Challenge
        self.lama_Challenge = lama_Challenge
        self.penjelasan = penjelasan
        self.constrains = constrains
        self.tujuan = tujuan
    

    }
    
}
