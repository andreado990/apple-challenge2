//
//  SeniorCell.swift
//  Apple Challnge 2
//
//  Created by Andre Marines ado Tena Uak on 22/04/20.
//  Copyright © 2020 Andre Marines ado Tena Uak. All rights reserved.
//

import UIKit

class SeniorCell: UICollectionViewCell {
    @IBOutlet weak var PhotoSenior: UIImageView!
    @IBOutlet weak var NameSenior: UILabel!
    @IBOutlet weak var JabatanSenior: UILabel!
    
}
