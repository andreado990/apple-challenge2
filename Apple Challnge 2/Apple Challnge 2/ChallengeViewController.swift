//
//  ChallengeViewController.swift
//  Apple Challnge 2
//
//  Created by Andre Marines ado Tena Uak on 14/04/20.
//  Copyright © 2020 Andre Marines ado Tena Uak. All rights reserved.
//

import UIKit


class ChallengeViewController: UIViewController {

    
    @IBOutlet weak var TableViewChallenge: UITableView!
    
    var Journey: [Data] = []
    
        
    var image = [UIImage(named: "challenge1"), UIImage(named: "challenge2"), UIImage(named: "challenge3"), UIImage(named: "challenge4"), UIImage(named: "challenge5"), UIImage(named: "challenge6"),]
        
    var name = ["Challenge 1", "Challenge 2", "Challenge 3", "Challenge 4", "Challenge 5", "Challenge 6"]
        
    var time = ["5 Week","2 Week", "4 Week", "4 Week", "2 Week", "12 Week" ]
        
    var Penjelasan = ["TBA","TBA","TBA","TBA","TBA","TBA", ]
        
    var Goals = ["TBA","TBA","TBA","TBA","TBA","TBA",]
        
    var Constrains = ["TBA","TBA","TBA","TBA","TBA","TBA",]
    
    var type = ["Team Challenge", "Individual Challenge", "Team Challenge", "Individual Challenge", "Team Challenge", "Team Challenge", ]
        
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        Journey = createArray()
        
        TableViewChallenge.delegate = self
        TableViewChallenge.dataSource = self
    }
    
    func createArray() ->  [Data] {
        
        var tempData: [Data] = []
        
        let data1 = Data(image: UIImage(named: "challenge1")!, title: "Challenge 1", waktu: "2 Week", penjelasan: "perkenalan awal", tipe: "Team Challenge", goals: "TBA", constrains: "No Game"  )
        let data2 = Data(image: UIImage(named: "challenge2")!,  title: "Challenge 2", waktu: "2 Week", penjelasan: "perkenalan awal", tipe: "Team Challenge", goals: "TBA", constrains: "No Game" )
        let data3 = Data(image: UIImage(named: "challenge3")!, title: "Challenge 3", waktu: "2 Week", penjelasan: "perkenalan awal", tipe: "Team Challenge", goals: "TBA", constrains: "No Game"  )
        let data4 = Data(image: UIImage(named: "challenge4")!, title: "Challenge 4", waktu: "2 Week", penjelasan: "perkenalan awal", tipe: "Team Challenge", goals: "TBA", constrains: "No Game"  )
        let data5 = Data(image: UIImage(named: "challenge5")!, title: "Challenge 5", waktu: "2 Week", penjelasan: "perkenalan awal", tipe: "Team Challenge", goals: "TBA", constrains: "No Game"  )
        let data6 = Data(image: UIImage(named: "challenge6")!, title: "Challenge 6", waktu: "2 Week", penjelasan: "perkenalan awal", tipe: "Team Challenge", goals: "TBA", constrains: "No Game"  )
        
        tempData.append(data1)
        tempData.append(data2)
        tempData.append(data3)
        tempData.append(data4)
        tempData.append(data5)
        tempData.append(data6)
        
        return tempData
        
    }

}

extension ChallengeViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Journey.count

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = Journey[indexPath.row]
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChallengeCell") as! ChallengeCell
            
            cell.setDetail(data : data)
            
            return cell
        }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "DetailChallenge") as? testViewController
        vc?.image = image[indexPath.row]!
        vc?.nama = name[indexPath.row]
        vc?.waktu = time[indexPath.row]
        vc?.penjelasan = Penjelasan[indexPath.row]
        vc?.goals = Goals[indexPath.row]
        vc?.constrains = Constrains[indexPath.row]
        vc?.tipe = type[indexPath.row]
        
        
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }

//    func table
    

        
}
